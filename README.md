![alt](https://i.ibb.co/ZH6GDsH/3.png)

## Le bundle simplonlua contient **une fonction verificator** qui renvoi le montant d'une subvention donnée perçue pour un lot donné.
Cette fonction prend **trois paramètres d'entrée** :

- un string au format Json comprenant les données liées au lot. (voir bundle serializer pour transformer les données d'une entité en données Json)
- le chemin vers le fichier Json Schema (.json) qui vérifie que la structure des données est bien respectée.
- le nom de la formule Lua (présente dans la table Subsidy) à utiliser pour calculer cette subvention. (en construction)



1 - composer require simplonlua/lua-calculator-bundle

2 - Créer un dossier à la racine de l'App qui contiendra le jsonSchema.json. ce fichier contient la structure attendue pour vos données clients (json).

3 - Créer un controller dans votre dossier src/controller.

4- Dans le controller, définir le path de votre dossier qui contient le Json Schema. 

```  
$webPath = $this->get('kernel')->getProjectDir() . '/votrenomdedossier/'; 

jsonSchema = realpath($webPath . 'votrenomdefichierschema.json');
```

5- Pour accèder au service : 
```         
$validJson = $this->container->get('lua.verificator')->verificator($jsonContent, $jsonSchema);
if ($validJson === true) {
            // $test = 'ok';

        } else {
        }
        return $this->render('validator/index.html.twig', [
	       'test' => $test,

            
        ]);
 ```


BONUS : Pour facilement génerer un string Json depuis une entité existante, vous pouvez utiliser le bundle serializer. 

Exemple : 
```    
$encoder = [new JsonEncoder()];
        $normalizer = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizer, $encoder);
        $owner = $this->getDoctrine()->getRepository(Owner::class)
                      ->findAll();
        $jsonContent = $serializer->serialize($owner, 'json');
        
```

