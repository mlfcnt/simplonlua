<?php
namespace simplonlua\LuaCalculatorBundle\Verificator;

use Opis\JsonSchema\Schema;
use Opis\JsonSchema\Validator;


class Verificator
{

    function verificator($jsonData, $jsonSchema )
    {
        
        // $dataJson = file_get_contents($jsonData);

        // $data = json_decode($dataJson);
        $schema = Schema::fromJsonString(file_get_contents($jsonSchema));

        $validator = new Validator();

        /** @var ValidationResult $result */
        $result = $validator->schemaValidation($jsonData, $schema);

        if ($result->isValid()) {
            return true;
        } else {
            /** @var ValidationError $error */
            $error = $result->getFirstError();
            echo '$data is invalid', PHP_EOL;
            echo "Error: ", $error->keyword(), PHP_EOL;
            echo json_encode($error->keywordArgs(), JSON_PRETTY_PRINT), PHP_EOL;
        }
    }

}
